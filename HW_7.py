#GAME

from random import randint

def yes_or_no():
    while True:
        user_input = input("You Win! One more time?: Yes or No: " )
        if user_input == "No":
            return "Good game!"
        if user_input == "Yes":
            return game()
        else:
            print("Must be 'Yes' or 'No'")

def game():
    random_number = randint(0,100)
    while True:
        try:
            guess_number = int(input("Input Your #: "))
            if guess_number == random_number:
                return  yes_or_no()
            if guess_number >= random_number + 10:
                print("Cold")
            if guess_number <= random_number - 10:
                print("Cold")
            if guess_number >= random_number + 5 and guess_number <= random_number + 10:
                print("Warm")
            if guess_number <= random_number - 5 and guess_number >= random_number - 10:
                print("Warm")
            if guess_number <= random_number - 1 and guess_number >= random_number - 4:
                print("Hot")
            if guess_number >= random_number + 1 and guess_number <= random_number + 4:
                print("Hot")
        except:
            print("NOT A NUMBER")


res = game()
print(res)

