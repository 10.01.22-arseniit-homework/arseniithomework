#Task

class Vehicle:
    engine = "Disel"
    body = "steel"
    speed = "60 km/h"
    model = "Tesla"

class Car(Vehicle):
    def limit(self):
        speed_limit = f'For {self.model} in Ukraine speed limit is {self.speed}'
        return speed_limit
    def new_car(self):
        new = f'Your new {self.model} with {self.body} body and {self.engine} engine is waiting outside. It easily can accelearate up to {self.speed} in few seconds'
        return new

    def __init__(self, model, engine, speed):
        self.engine = engine
        self.speed = speed
        self.model = model

lada = Car(model="lada",engine="Gas", speed="30 km/h")
print(lada.limit())
tesla = Car(model="Tesla", engine="Electric", speed="100 km/h")
print(tesla.new_car())

class Plane(Vehicle):
    def info(self):
        rules_fly = f'For {self.model} if plane has {self.body} body and {self.engine} engine, it must have {self.speed} to fly'
        return rules_fly

plane1 = Plane()
plane1.engine = "Diesel"
plane1.model = "Boeing"
plane1.body = "Carbon"
plane1.speed = "250 km/h"
print(plane1.info())

plane2 = Plane()
plane2.engine = "H2 powered"
plane2.model = "DLR-H2"
plane2.body = "Steel"
plane2.speed = "180 km/h"
print(plane2.info())

class Ship(Vehicle):
    def unknown(self):
        from_where = f'This {self.body} {self.model} has {self.engine} power with max speed {self.speed} knots'
        return from_where

    def __init__(self, knots):
        self.speed = knots


ship1 = Ship(knots=10)
ship1.engine = "Rowers"
ship1.model = "Frigate"
ship1.body = "Wood"
print(ship1.unknown())





