from random import choice as random_choice
from random import shuffle
from datetime import datetime

def user_choice(*variants):
    msg = f'Choose one of: {", ".join(variants)}: '
    while True:
        user_input = input(msg)
        if user_input not in variants:
            print("Not in the variants!")
        else:
            return user_input


def computer_choice(*variants):
    variants = list(variants)
    shuffle(variants)

    result = random_choice(variants)

    return result
def is_user_win(rules_to_win, user_figure, computer_figure):

    if user_figure == computer_figure:
        return
    if computer_figure in rules_to_win[user_figure]:
        return True

    return False

def make_message(result, user_figure, computer_figure):
    dct = {
        True: "USER WIN!",
        False: "COMPUTER WIN!",
        None: "DRAW!",
    }
    massage_in_make_massage = f'User - {user_figure}, computer - {computer_figure}, result is - {dct[result]}'
    return massage_in_make_massage

def RSPLS_game():

    counter = 0
    with open('text.txt', 'a') as file:
        while True:
            file.write(str(t))

            counter += 1

            figures = ("Rock", "Scissors", "Paper", "Lizard", "Spock")

            user_figure = user_choice(*figures)
            computer_figure = computer_choice(*figures)

            rules_to_win = {
                "Rock": ["Scissors", "Lizard"],
                "Scissors": ["Paper", "Lizard"],
                "Paper": ["Rock", "Spock"],
                "Lizard": ["Spock", "Paper"],
                "Spock": ["Scissors", "Rock"],

            }
            result = is_user_win(rules_to_win, user_figure, computer_figure)

            massage = make_message(result, user_figure, computer_figure)
            current_time = datetime.now()

            t = counter, str(current_time), massage
            print(t)
