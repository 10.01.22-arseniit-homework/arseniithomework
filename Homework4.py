#Task 1

str = "Есть строка произвольного содержания"
str_split = str.split()
v = ['а', 'у', 'о', 'ы', 'и', 'э', 'я', 'ю', 'ё', 'е', 'й', 'ь']
for word in str_split:
    longest = max(str.split(), key=len)
    counter = 0
    short_word = ''
    add_word = ''
    for letter in word:
        if letter in v:
            counter += 1
        else:
            counter = 0
        if counter == 2:
            add_word = word
    if len(add_word) >= len(longest):
        print(add_word)

#Task 2

lower_limit = 37.9
upper_limit = 40.2

my_dict = { "citrus": 47.999,
            "istudio": 42.999,
            "moyo": 49.999,
            "royal-service": 37.245,
            "buy.ua": 38.324,
            "g-store": 37.166,
            "ipartner": 38.988,
            "sota": 37.720,
            "rozetka": 38.003}
res = dict()
for key, val in my_dict.items():
    if int(val) >= lower_limit and int(val) <= upper_limit:
        print("Match: ", key)


